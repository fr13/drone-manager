package dev.fr13.dronemanager.controller;

import dev.fr13.dronemanager.dto.*;
import dev.fr13.dronemanager.validatior.DroneSerialNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class DroneRestController {

    @PostMapping("/drones")
    @ResponseStatus(HttpStatus.CREATED)
    public RegisterDroneResponse registerDrone(@RequestBody @Valid RegisterDroneRequest request) {
        //add log
        return null;
    }

    @GetMapping("/drones")
    public List<DroneDto> getDrones(@RequestParam DroneStatus status) {
        //add test
        return null;
    }

    @GetMapping("/drones/{serialNumber}")
    public DroneDto getDrone(@PathVariable @DroneSerialNumber String serialNumber) {
        //add test
        return null;
    }

    @PostMapping("/deliveries")
    @ResponseStatus(HttpStatus.CREATED)
    public DeliveryDto createDelivery(@RequestBody @Valid CreateDeliveryRequest request) {
        //fix success test to check the body
        //lock by drone
        return null;
    }

    @GetMapping("/deliveries")
    public DeliveryDto getDroneDelivery(@RequestParam(name = "drone-serial-number")
                                        @DroneSerialNumber String droneSerialNumber) {
        //fix success test to check the body
        return null;
    }
}