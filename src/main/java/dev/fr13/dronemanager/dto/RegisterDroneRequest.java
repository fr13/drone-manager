package dev.fr13.dronemanager.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@Builder
public class RegisterDroneRequest {

    @NotNull
    @Size(min = 1, max = 100, message = "The serial number ${validatedValue} must be between {min} and {max} characters long")
    private String serialNumber;

    @NotNull(message = "The model must not be null")
    private DroneModel model;

    @Min(value = 0, message = "The battery capacity ${validatedValue} must be greater than 0")
    @Max(value = 100, message = "The battery capacity ${validatedValue} must be less than 100")
    private int batteryCapacity;
}