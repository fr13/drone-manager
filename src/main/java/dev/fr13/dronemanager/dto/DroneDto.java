package dev.fr13.dronemanager.dto;

import lombok.Data;

@Data
public class DroneDto {

    private String serialNumber;
    private DroneModel model;
    private int weightLimit;
    private int batteryCapacity;
}