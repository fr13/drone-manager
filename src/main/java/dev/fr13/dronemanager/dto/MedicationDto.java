package dev.fr13.dronemanager.dto;

import lombok.Data;

@Data
public class MedicationDto {

    private String name;
    private String weight;
    private String code;
    private String imageUrl;
}