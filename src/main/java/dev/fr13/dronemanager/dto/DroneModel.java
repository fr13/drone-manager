package dev.fr13.dronemanager.dto;

public enum DroneModel {

    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT
}