package dev.fr13.dronemanager.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ApiErrorResult {

    private int status;
    private LocalDateTime timestamp;
    private String message;
    private String url;
    private List<String> fieldErrors;
}