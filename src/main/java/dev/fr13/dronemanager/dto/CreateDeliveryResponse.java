package dev.fr13.dronemanager.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class CreateDeliveryResponse {

    private UUID id;
    private DroneDto drone;
    private List<MedicationDto> medications;
    private Total total;

    @Data
    public static class Total {
        private int weight;
        private int items;
    }
}