package dev.fr13.dronemanager.dto;

import dev.fr13.dronemanager.validatior.DroneSerialNumber;
import dev.fr13.dronemanager.validatior.MedicationCodes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Collections;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateDeliveryRequest {

    @DroneSerialNumber
    @Getter
    private String droneSerialNumber;

    @MedicationCodes
    @NotEmpty(message = "The medication codes must not be empty")
    private List<String> medicationCodes;

    public List<String> getMedicationCodes() {
        return Collections.unmodifiableList(medicationCodes);
    }
}