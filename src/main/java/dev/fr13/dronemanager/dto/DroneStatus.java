package dev.fr13.dronemanager.dto;

public enum DroneStatus {

    AVAILABLE, BUSY, MAINTENANCE
}