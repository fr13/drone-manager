package dev.fr13.dronemanager.util;

import lombok.experimental.UtilityClass;

import java.util.Objects;
import java.util.regex.Pattern;

@UtilityClass
public class RegExUtils {

    private static final Pattern DRONE_SERIAL_PATTERN = Pattern.compile("^.{1,100}");
    private static final Pattern MEDICATION_CODE_PATTERN = Pattern.compile("^[A-Z]+(?:_\\d+)*$");

    public static boolean isValidMedicationCode(String code) {
        String val = Objects.requireNonNullElse(code, "");
        return MEDICATION_CODE_PATTERN.matcher(val).matches();
    }

    public static boolean isValidDroneSerialNumber(String serialNumber) {
        String val = Objects.requireNonNullElse(serialNumber, "");
        return !val.isBlank() && DRONE_SERIAL_PATTERN.matcher(val).matches();
    }
}