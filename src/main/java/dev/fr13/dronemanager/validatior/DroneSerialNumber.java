package dev.fr13.dronemanager.validatior;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = DroneSerialNumberValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface DroneSerialNumber {

    String message() default "The serial number must be 100 characters max long";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}