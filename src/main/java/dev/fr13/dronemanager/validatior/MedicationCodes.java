package dev.fr13.dronemanager.validatior;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Constraint(validatedBy = MedicationCodeValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface MedicationCodes {

    String message() default "The medication code can contain upper case letters underscore and numbers (i.e. ABC_55)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}