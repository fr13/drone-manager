package dev.fr13.dronemanager.validatior;

import dev.fr13.dronemanager.util.RegExUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DroneSerialNumberValidator implements ConstraintValidator<DroneSerialNumber, String> {

    @Override
    public boolean isValid(String serialNumber, ConstraintValidatorContext context) {
        return RegExUtils.isValidDroneSerialNumber(serialNumber);
    }
}