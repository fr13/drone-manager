package dev.fr13.dronemanager.validatior;

import dev.fr13.dronemanager.util.RegExUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class MedicationCodeValidator implements ConstraintValidator<MedicationCodes, List<String>> {

    @Override
    public boolean isValid(List<String> codes, ConstraintValidatorContext context) {
        return codes.stream().allMatch(RegExUtils::isValidMedicationCode);
    }
}