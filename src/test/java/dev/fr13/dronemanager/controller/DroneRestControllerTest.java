package dev.fr13.dronemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.fr13.dronemanager.dto.DroneModel;
import dev.fr13.dronemanager.dto.CreateDeliveryRequest;
import dev.fr13.dronemanager.dto.RegisterDroneRequest;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DroneRestControllerTest {

    private static final String DRONE_SERIAL_NUMBER = "X-100";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @SneakyThrows
    void shouldFailToRegisterIfDroneFieldsNull() {
        RegisterDroneRequest request = RegisterDroneRequest.builder().build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(2)));
    }

    @Test
    @SneakyThrows
    void shouldFailToRegisterIfSerialNumberIsEmpty() {
        RegisterDroneRequest request = RegisterDroneRequest.builder()
                .serialNumber("")
                .model(DroneModel.LIGHTWEIGHT)
                .batteryCapacity(100)
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", startsWith("The serial number")));
    }

    @Test
    @SneakyThrows
    void shouldFailToRegisterIfModelIsNull() {
        RegisterDroneRequest request = RegisterDroneRequest.builder()
                .serialNumber(DRONE_SERIAL_NUMBER)
                .batteryCapacity(100)
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", is("The model must not be null")));
    }

    @Test
    @SneakyThrows
    void shouldRegisterDrone() {
        RegisterDroneRequest request = RegisterDroneRequest.builder()
                .serialNumber("X100")
                .model(DroneModel.HEAVYWEIGHT)
                .batteryCapacity(100)
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    @SneakyThrows
    void shouldFailToCreateDeliveryIfMedicationCodesEmpty() {
        CreateDeliveryRequest request = CreateDeliveryRequest.builder()
                .droneSerialNumber(DRONE_SERIAL_NUMBER)
                .medicationCodes(List.of())
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/deliveries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", is("The medication codes must not be empty")));
    }

    @Test
    @SneakyThrows
    void shouldFailToCreateDeliveryIfMedicationCodesInvalid() {
        CreateDeliveryRequest request = CreateDeliveryRequest.builder()
                .droneSerialNumber(DRONE_SERIAL_NUMBER)
                .medicationCodes(List.of("ABC_100", "", "abc"))
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/deliveries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", startsWith("The medication code can contain")));
    }

    @Test
    @SneakyThrows
    void shouldFailToCreateDeliveryIfDroneSerialNumberInvalid() {
        CreateDeliveryRequest request = CreateDeliveryRequest.builder()
                .droneSerialNumber(" ")
                .medicationCodes(List.of("ABC_100"))
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/deliveries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", is("The serial number must be 100 characters max long")));
    }

    @Test
    @SneakyThrows
    void shouldGetAvailableDrones() {
        mockMvc.perform(get("/api/v1/drones?status=AVAILABLE"))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    void shouldCreateDelivery() {
        CreateDeliveryRequest request = CreateDeliveryRequest.builder()
                .droneSerialNumber(DRONE_SERIAL_NUMBER)
                .medicationCodes(List.of("ABC_100", "ABC_101"))
                .build();
        String json = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/api/v1/deliveries")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isCreated());
    }

    @Test
    @SneakyThrows
    void shouldFailToGetDeliveryIfDroneSerialNumberInvalid() {
        String droneSerialNumberTooLong = IntStream.range(1, 80).mapToObj(String::valueOf).collect(Collectors.joining());
        mockMvc.perform(get("/api/v1/deliveries?drone-serial-number=" + droneSerialNumberTooLong))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.fieldErrors", hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0]", is("The serial number must be 100 characters max long")));
    }

    @Test
    @SneakyThrows
    void shouldGetDelivery() {
        mockMvc.perform(get("/api/v1/deliveries/?drone-serial-number=" + DRONE_SERIAL_NUMBER))
                .andExpect(status().isOk());
    }
}